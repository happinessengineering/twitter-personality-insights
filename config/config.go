package config

var (

	// Version - application version
	Version = "0.0.2"

	// ConfigFileName - the name of the configuration file.
	ConfigFileName = "config.yaml"

	// IbmAPIPath - API Path suffix
	IbmAPIPath = "/v3/profile"

	// IbmAPIQueryStrings - query strings to add to the request
	IbmAPIQueryStrings = "version=2017-10-13&consumption_preferences=true&raw_scores=true"
)
