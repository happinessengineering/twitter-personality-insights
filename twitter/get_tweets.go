package twitter

import (
	"github.com/dghubble/go-twitter/twitter"
	log "github.com/sirupsen/logrus"
)

// GetTweets - get all the tweets
func GetTweets(count int, username string, includeRT bool, client *twitter.Client) []twitter.Tweet {

	maxid := int64(0)
	var tweets []twitter.Tweet
	var allTweets []twitter.Tweet
	var err error

	// Twitter `user_timeline` API limits results to 3200 tweets.
	// Fetch in batches, according to `count`
	for i := 0; i <= 15; i++ {

		if i == 0 {

			tweets, _, err = client.Timelines.UserTimeline(
				&twitter.UserTimelineParams{
					ScreenName:      username,
					Count:           count,
					IncludeRetweets: &includeRT,
				},
			)
			if err != nil {
				log.Error("Could not get tweets: ", err)
				return allTweets
			}

		} else {

			tweets, _, err = client.Timelines.UserTimeline(
				&twitter.UserTimelineParams{
					ScreenName:      username,
					Count:           count,
					IncludeRetweets: &includeRT,
					MaxID:           maxid,
				},
			)
			if err != nil {
				log.Error("Could not get tweets: ", err)
				return allTweets
			}

			// Move the MaxID by one so the next loop
			// picks up in the right spot.
			if len(tweets) > 0 {
				maxid = tweets[len(tweets)-1].ID - 1
			}
		}

		for x := range tweets {
			allTweets = append(allTweets, tweets[x])
		}

	}

	return allTweets

}
