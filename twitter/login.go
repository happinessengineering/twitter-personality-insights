package twitter

import (
	"github.com/dghubble/oauth1"

	"github.com/dghubble/go-twitter/twitter"
)

// Login - login to twitter and return an authorized client
func Login(consumerKey, consumerSecret, accessToken, accessTokenSecret string) *twitter.Client {

	config := oauth1.NewConfig(consumerKey, consumerSecret)
	token := oauth1.NewToken(accessToken, accessTokenSecret)

	httpClient := config.Client(oauth1.NoContext, token)

	client := twitter.NewClient(httpClient)

	return client

}
