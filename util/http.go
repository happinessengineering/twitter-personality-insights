package util

import (
	"bytes"
	"encoding/base64"
	"log"
	"net/http"
)

// HTTPRequest - Make a http request.
func HTTPRequest(j []byte, verb, url string, headers []map[string]string) *http.Response {

	req, err := http.NewRequest(verb, url, bytes.NewBuffer(j))
	req.Header.Set("Content-Type", "application/json")

	if len(headers) > 0 {
		for idx := range headers {
			req.Header.Set(headers[idx]["name"], headers[idx]["value"])
		}
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	return resp

}

// BasicAuth - create the basic auth string
func BasicAuth(u, p string) string {

	authString := u + ":" + p
	return base64.StdEncoding.EncodeToString([]byte(authString))

}
