package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/spf13/cobra"

	"github.com/briandowns/spinner"
	log "github.com/sirupsen/logrus"

	cfg "gitlab.com/graphik/twitter-personality-insights/config"
	"gitlab.com/graphik/twitter-personality-insights/pi"
	"gitlab.com/graphik/twitter-personality-insights/twitter"
	"gitlab.com/graphik/twitter-personality-insights/util"
)

var queryCmd = &cobra.Command{
	Use:   "query",
	Short: "query the personality insights service",
	Long:  "",

	Run: runQuery,
}

func runQuery(cmd *cobra.Command, args []string) {

	if len(username) <= 0 {
		log.Error("Must include a username!")
		return
	}

	// Init the spinner.
	s := spinner.New(spinner.CharSets[14], 100*time.Millisecond)

	fmt.Println(fmt.Sprintf("Fetching tweets for user: @%s", username))

	// Begin spinning
	s.Start()

	tClient := twitter.Login(
		userConfig.TwitterAPIKey,
		userConfig.TwitterSecretKey,
		userConfig.TwitterAccessToken,
		userConfig.TwitterAccessTokenSecret,
	)

	tweets := twitter.GetTweets(
		200,
		username,
		includeRetweets,
		tClient,
	)

	// Stop the spinner.
	s.Stop()

	fmt.Println(fmt.Sprintf("Found %d tweets for user: @%s", len(tweets), username))

	if len(tweets) == 0 {
		return
	}

	fmt.Println(fmt.Sprintf("Creating CI payload and sending request to: %s", userConfig.IbmURL))

	s.Start()

	ciPayload := pi.CreateCIPayload(
		tweets,
	)

	if writeInput {
		_ = ioutil.WriteFile(username+"-input.json", ciPayload, 0644)
	}

	resp := util.HTTPRequest(
		[]byte(ciPayload),
		"POST",
		userConfig.IbmURL+cfg.IbmAPIPath+"?"+cfg.IbmAPIQueryStrings,
		[]map[string]string{
			{
				"name":  "Authorization",
				"value": "Basic " + util.BasicAuth("apikey", userConfig.IbmAPIKey),
			},
			{
				"name":  "Accept",
				"value": "application/json",
			},
		},
	)

	// Read the response body.
	body, _ := ioutil.ReadAll(resp.Body)

	// Marshal the response into the PI response struct
	var piResponse pi.APIResponse
	err := json.Unmarshal(body, &piResponse)
	if err != nil {
		log.Error("Could not unmarshal json response: ", err)
		return
	}

	s.Stop()

	if export {

		util.ExportToJSON(piResponse, username)

	}

	fmt.Println("")
	fmt.Println("Personality Insights")
	fmt.Println("--------------------")
	fmt.Println("")
	fmt.Println(fmt.Sprintf("Username: @%s", username))
	fmt.Println(fmt.Sprintf("Total words processed: %d", piResponse.WordCount))

	printProfile(piResponse)
	printNeeds(piResponse)
	printValues(piResponse)
	printBehaviors(piResponse)
	printConsumptionPreferences(piResponse)

}

func printProfile(apiResponse pi.APIResponse) {

	fmt.Print(fmt.Sprintf("\nProfile\n-----\n"))

	tabOut := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)

	for _, item := range apiResponse.Personality {

		percent := strconv.FormatFloat(item.Percentile*100, 'f', 2, 64)
		score := strconv.FormatFloat(item.RawScore, 'f', 5, 64)

		fmt.Fprintln(
			tabOut, fmt.Sprintf(
				"Trait: %s\nPercentile: %s%%\nRaw Score: %s",
				item.Name, percent, score,
			))

		if len(item.Children) > 0 && !concise {

			fmt.Fprintln(tabOut, fmt.Sprintf("Breakdown:"))

			for _, child := range item.Children {

				percent := strconv.FormatFloat(child.Percentile*100, 'f', 2, 64)
				score := strconv.FormatFloat(child.RawScore, 'f', 5, 64)

				fmt.Fprintln(
					tabOut, fmt.Sprintf("  %s\t%s%%\t%s", child.Name, percent, score))

			}

			fmt.Fprintln(tabOut, fmt.Sprintf("\n"))

		}

	}
	tabOut.Flush()

}

func printValues(apiResponse pi.APIResponse) {

	fmt.Print(fmt.Sprintf("\nValues\n-----\n\n"))

	tabOut := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
	fmt.Fprintln(
		tabOut, fmt.Sprintf("%s\t%s\t%s\t%s", "Name", "Percentile", "Raw Score", "Is Significant"))
	for _, v := range apiResponse.Values {
		percent := strconv.FormatFloat(v.Percentile*100, 'f', 2, 64)
		score := strconv.FormatFloat(v.RawScore, 'f', 5, 64)
		fmt.Fprintln(
			tabOut, fmt.Sprintf("%s:\t%s%%\t%s\t%t", v.Name, percent, score, v.Significant))
	}
	tabOut.Flush()
	fmt.Println("")

}

func printNeeds(apiResponse pi.APIResponse) {

	fmt.Print(fmt.Sprintf("\nNeeds\n-----\n\n"))

	tabOut := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
	fmt.Fprintln(
		tabOut, fmt.Sprintf("%s\t%s\t%s\t%s", "Name", "Percentile", "Raw Score", "Is Significant"))
	for _, n := range apiResponse.Needs {
		percent := strconv.FormatFloat(n.Percentile*100, 'f', 2, 64)
		score := strconv.FormatFloat(n.RawScore, 'f', 5, 64)
		fmt.Fprintln(
			tabOut, fmt.Sprintf("%s:\t%s%%\t%s\t%t", n.Name, percent, score, n.Significant))
	}
	tabOut.Flush()
	fmt.Println("")

}

func printBehaviors(apiResponse pi.APIResponse) {

	fmt.Print(fmt.Sprintf("\nBehavior Distribution\n-----\n\n"))

	days := []string{}
	hours := []string{}

	daysTable := util.TableCreator([]string{
		"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday",
	})
	daysTable.SetBorder(false)
	daysTable.SetHeaderLine(false)
	daysTable.SetRowLine(false)

	hoursTable := util.TableCreator([]string{
		"12:00 am", "01:00 am", "02:00 am", "03:00 am", "04:00 am", "05:00 am", "06:00 am", "07:00 am", "08:00 am", "09:00 am", "10:00 am", "11:00 am",
	})
	hoursTable.SetBorder(false)
	hoursTable.SetHeaderLine(false)
	hoursTable.SetRowLine(false)

	hoursTableEvening := util.TableCreator([]string{
		"12:00 pm", "01:00 pm", "02:00 pm", "03:00 pm", "04:00 pm", "05:00 pm", "06:00 pm", "07:00 pm", "08:00 pm", "09:00 pm", "10:00 pm", "11:00 pm",
	})
	hoursTableEvening.SetBorder(false)
	hoursTableEvening.SetHeaderLine(false)
	hoursTableEvening.SetRowLine(false)

	for _, b := range apiResponse.Behavior {
		percent := strconv.FormatFloat(b.Percentage*100, 'f', 2, 64)
		if b.Name == "Monday" || b.Name == "Tuesday" || b.Name == "Wednesday" || b.Name == "Thursday" || b.Name == "Friday" || b.Name == "Saturday" || b.Name == "Sunday" {
			days = append(days, percent)
		} else {
			hours = append(hours, percent)
		}
	}

	daysTable.Append(days)

	if len(hours) > 12 {
		hoursTable.Append(hours[:12])
		hoursTableEvening.Append(hours[12:])
	}

	daysTable.Render()
	fmt.Println("")
	hoursTable.Render()
	fmt.Println("")
	hoursTableEvening.Render()
	fmt.Println("")

}

func printConsumptionPreferences(apiResponse pi.APIResponse) {

	fmt.Print(fmt.Sprintf("\nConsumption Behaviors\n-----\n\n"))
	tabOut := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)

	unlikely := []pi.Preference{}
	neutral := []pi.Preference{}
	likely := []pi.Preference{}

	for _, cp := range apiResponse.ConsumptionPreferences {

		for _, p := range cp.Preferences {

			if p.Score == 0 {
				unlikely = append(unlikely, p)
			} else if p.Score == 0.5 {
				neutral = append(neutral, p)
			} else if p.Score == 1 {
				likely = append(likely, p)
			}
		}

	}

	if len(unlikely) > 0 {
		fmt.Fprintln(tabOut, fmt.Sprintf("Unlikely to..."))
		for _, u := range unlikely {
			trimName := strings.TrimPrefix(u.Name, "Likely to ")
			fmt.Fprintln(
				tabOut, fmt.Sprintf("... %s", trimName))
		}
		fmt.Fprintln(tabOut, "")
	}

	if len(neutral) > 0 {
		fmt.Fprintln(tabOut, fmt.Sprintf("Neutral to..."))
		for _, n := range neutral {
			trimName := strings.TrimPrefix(n.Name, "Likely to ")
			fmt.Fprintln(
				tabOut, fmt.Sprintf("... %s", trimName))
		}
		fmt.Fprintln(tabOut, "")
	}

	if len(likely) > 0 {
		fmt.Fprintln(tabOut, fmt.Sprintf("Likely to..."))
		for _, l := range likely {
			trimName := strings.TrimPrefix(l.Name, "Likely to ")
			fmt.Fprintln(
				tabOut, fmt.Sprintf("... %s", trimName))
		}
		fmt.Fprintln(tabOut, "")
	}

	tabOut.Flush()
	fmt.Println("")

}
